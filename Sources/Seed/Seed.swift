// copyright: Jan Mazurczak 2014-2018

import Foundation


public typealias SeedSource = UnsignedInteger & FixedWidthInteger


open class Seed<Source: SeedSource> {
    
    public let data: Data
    public init(data: Data) { self.data = data }
    public init(string: String) { self.data = string.data() }
    
    private var sourceNumbers = [Source]()
    private var sourceModifier: UInt64 = 0
    public var precachedNumbersCount: Int { return sourceNumbers.count }
    public private(set) var utilisedNumbersCount: Int = 0
    
    private func refill() {
        sourceNumbers = sourceNumbers + (data + sourceModifier).sha256().readValues()
        sourceModifier += 1
    }
    
    var numberOfValuesInOneRead: Int {
        let elementSize = MemoryLayout<Source>.size
        return 32 / elementSize // 32 stands for length of sha256 in bytes
    }
    
    open func read<Output>(_ reader: SeedReader<Source, Output>) -> Output {
        while sourceNumbers.count < reader.numbersNeeded {
            refill()
        }
        let source = Array(sourceNumbers.prefix(upTo: reader.numbersNeeded))
        sourceNumbers.removeFirst(reader.numbersNeeded)
        utilisedNumbersCount += reader.numbersNeeded
        
        let result = reader.outputConverter(source)
        return result
    }
    
    open func read<Output>(_ reader: SeedReader<Source, Output>, amount: Int) -> [Output] {
        var results = [Output]()
        while results.count < amount {
            results.append(read(reader))
        }
        return results
    }
    
    open func reset() {
        sourceNumbers = []
        sourceModifier = 0
        utilisedNumbersCount = 0
    }
    
    open func restore(utilisedNumbersCount: Int) {
        reset()
        
        sourceModifier = UInt64(utilisedNumbersCount / numberOfValuesInOneRead)
        
        let toUtilise = utilisedNumbersCount % numberOfValuesInOneRead
        if toUtilise > 0 {
            refill()
            sourceNumbers.removeFirst(toUtilise)
        }
        self.utilisedNumbersCount = utilisedNumbersCount
    }
    
}


public extension Data {
    
    func readValues<T: BinaryInteger>() -> [T] {
        let elementSize = MemoryLayout<T>.size
        let numberOfElements = count / elementSize
        guard numberOfElements > 0 else { return [] }
        
        var result: [T] = []
        for i in 0..<numberOfElements {
            let elementData = subdata(in: (i * elementSize)..<((i + 1) * elementSize))
            result.append(elementData.toInteger())
        }
        return result
    }
    
}
