// Copyright: Jan Mazurczak 2019

public struct SeedsBridgeReader<Source, Output> where Source: SeedSource {
    
    public let reader: SeedReader<Source, Output>
    public let bridgeCombining: (Output, Output, Double) -> Output
    
    public init(_ reader: SeedReader<Source, Output>, bridgeCombining: @escaping (Output, Output, Double) -> Output) {
        self.reader = reader
        self.bridgeCombining = bridgeCombining
    }
    
}
