//
//  Random.swift
//  CommonElements
//
//  Created by Jan Mazurczak on 21/08/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//

import Foundation


extension Array {
    
    
    public func random(_ length: Int) -> [Element] {
        
        if count == 0 { return [] }
        
        var result = [Element]()
        for _ in 0..<length {
            let pickedIndex = Index.random(in: 0..<count)
            let picked = self[pickedIndex]
            result.append(picked)
        }
        return result
    }
    
    
}


open class RandomNotRepeatingValuesServer<T> {
    
    
    public init(_ allValues: [T]) {
        self.allValues = allValues
    }
    
    
    private let allValues: [T]
    private var values = [T]()
    
    
    private func refillIfNeeded() {
        if values.isEmpty { values = allValues }
    }
    
    
    open func getOne() -> T {
        refillIfNeeded()
        let index = [T].Index.random(in: 0..<values.count)
        return values.remove(at: index)
    }
    
}
