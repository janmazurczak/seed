// Copyright: Jan Mazurczak 2019

import Foundation

open class TimeSeed<Source>: MultidimensionalSeedsBridge<Source> where Source: SeedSource {
    
    public init(sliceDuration: Int, modifier: String, date: Date) {
        super.init(dimensions: [DimensionInfo(sliceLength: sliceDuration, defaultPosition: 0)], seedModifier: modifier, position: [date.timeIntervalSinceReferenceDate])
    }
    
    public func update(date: Date) {
        update(to: [date.timeIntervalSinceReferenceDate])
    }
    
}
