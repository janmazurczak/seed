// Copyright: Jan Mazurczak 2019

import UIKit

extension CAMediaTiming {
    
    public func synchroniseTiming<Source>(with seed: MultidimensionalSeedsBridge<Source>, at dimensionIndex: Int, currentMediaTime: CFTimeInterval) {
        
        let duration = CFTimeInterval(seed.dimensions[dimensionIndex].sliceLength)
        self.duration = duration
        self.beginTime = currentMediaTime - (CFTimeInterval(seed.segmentRelativePosition[dimensionIndex]) * duration)
        
    }
    
}
