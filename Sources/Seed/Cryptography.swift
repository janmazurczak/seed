// Copyright: Jan Mazurczak 2019

import Foundation
import CryptoKit

public extension Data {
    
    func sha256() -> Data {
        Data(SHA256.hash(data: self))
    }
    
}
