// Copyright: Jan Mazurczak 2019

import Foundation

public struct DimensionInfo {
    public let sliceLength: Int
    public let defaultPosition: Double
    
    public init(sliceLength: Int, defaultPosition: Double) {
        self.sliceLength = sliceLength
        self.defaultPosition = defaultPosition
    }
    
    func sliceEdges(for position: Double) -> (Int, Int) {
        let a = Int(floor(position / Double(sliceLength))) * sliceLength
        let b = a + sliceLength
        return (a, b)
    }
}

open class MultidimensionalSeedsBridge<Source> where Source: SeedSource {
    
    enum Segment {
        indirect case interdimensional(Segment, Segment)
        case dimensional(SeedsBridge<Source>)
        
        static func create(in sliceEdges: [(Int, Int)], offset: Int = 0, prefix: String) -> Segment {
            let a = prefix + " \(offset): \(sliceEdges[offset].0)"
            let b = prefix + " \(offset): \(sliceEdges[offset].1)"
            let nextOffset = offset + 1
            if nextOffset < sliceEdges.count {
                return .interdimensional(
                    create(in: sliceEdges, offset: nextOffset, prefix: a),
                    create(in: sliceEdges, offset: nextOffset, prefix: b)
                )
            } else {
                return .dimensional(SeedsBridge(
                    seedA: Seed(string: a),
                    seedB: Seed(string: b)))
            }
        }
        
        var allSeeds: [SeedsBridge<Source>] {
            switch self {
            case .dimensional(let seed): return [seed]
            case .interdimensional(let seedA, let seedB): return seedA.allSeeds + seedB.allSeeds
            }
        }
        
        func read<Output>(_ reader: SeedsBridgeReader<Source, Output>, position: [Double]) -> Output {
            var position = position
            let progress = position.removeFirst()
            switch self {
            case .dimensional(let seed):
                seed.progress = progress
                return seed.read(reader)
            case .interdimensional(let segmentA, let segmentB):
                let a = segmentA.read(reader, position: position)
                let b = segmentB.read(reader, position: position)
                return reader.bridgeCombining(a, b, progress)
            }
        }
    }
    
    public let dimensions: [DimensionInfo]
    public let seedModifier: String
    private var segment: Segment
    public private(set) var segmentRelativePosition: [Double]
    public private(set) var segmentEdges: [(Int, Int)]
    
    public init(dimensions: [DimensionInfo], seedModifier: String, position: [Double]) {
        self.dimensions = dimensions
        self.seedModifier = seedModifier
        self.segment = .dimensional(SeedsBridge(seedA: Seed(string: ""), seedB: Seed(string: "")))
        self.segmentRelativePosition = []
        self.segmentEdges = []
        update(to: position)
    }
    
    open func updated(to position: [Double]) -> MultidimensionalSeedsBridge {
        let result = MultidimensionalSeedsBridge(dimensions: dimensions, seedModifier: seedModifier, position: position)
        result.restore(utilisedNumbersCount: utilisedNumbersCount)
        return result
    }
    
    open func update(to position: [Double]) {
        let utilised = segment.allSeeds.map{ $0.utilisedNumbersCount }.max() ?? 0
        let filledPosition = dimensions.enumerated().map {
            ($0.offset < position.count) ? position[$0.offset] : $0.element.defaultPosition
        }
        segmentEdges = dimensions.enumerated().map {
            $0.element.sliceEdges(for: filledPosition[$0.offset])
        }
        segment = .create(in: segmentEdges, prefix: seedModifier)
        restore(utilisedNumbersCount: utilised)
        
        segmentRelativePosition = dimensions.enumerated().map {
            (filledPosition[$0.offset] - Double(segmentEdges[$0.offset].0)) / Double($0.element.sliceLength)
        }
    }
    
    public var utilisedNumbersCount: Int {
        return segment.allSeeds.map{ $0.utilisedNumbersCount }.max() ?? 0
    }
    
    public func sync() {
        restore(utilisedNumbersCount: utilisedNumbersCount)
    }
    
    open func read<Output>(_ reader: SeedsBridgeReader<Source, Output>) -> Output {
        sync()
        return segment.read(reader, position: segmentRelativePosition)
    }
    
    open func read<Output>(_ reader: SeedsBridgeReader<Source, Output>, amount: Int) -> [Output] {
        var results = [Output]()
        while results.count < amount {
            results.append(read(reader))
        }
        return results
    }
    
    open func reset() {
        segment.allSeeds.forEach { $0.reset() }
    }
    
    open func restore(utilisedNumbersCount: Int) {
        segment.allSeeds.forEach {
            $0.restore(utilisedNumbersCount: utilisedNumbersCount)
        }
    }
    
}
