// Copyright: Jan Mazurczak 2019

open class SeedsBridge<Source> where Source: SeedSource {
    
    public var seedA: Seed<Source>
    public var seedB: Seed<Source>
    public var progress: Double = 0.5
    
    public init(seedA: Seed<Source>, seedB: Seed<Source>) {
        self.seedA = seedA
        self.seedB = seedB
        sync()
    }
    
    public func sync() {
        let utilised = utilisedNumbersCount
        restore(utilisedNumbersCount: utilised)
    }
    
    open func read<Output>(_ reader: SeedsBridgeReader<Source, Output>) -> Output {
        sync()
        let a = seedA.read(reader.reader)
        let b = seedB.read(reader.reader)
        return reader.bridgeCombining(a, b, progress)
    }
    
    open func read<Output>(_ reader: SeedsBridgeReader<Source, Output>, amount: Int) -> [Output] {
        var results = [Output]()
        while results.count < amount {
            results.append(read(reader))
        }
        return results
    }
    
    open func reset() {
        seedA.reset()
        seedB.reset()
    }
    
    open var utilisedNumbersCount: Int {
        return max(seedA.utilisedNumbersCount, seedB.utilisedNumbersCount)
    }
    
    open func restore(utilisedNumbersCount: Int) {
        if seedA.utilisedNumbersCount != utilisedNumbersCount {
            seedA.restore(utilisedNumbersCount: utilisedNumbersCount)
        }
        if seedB.utilisedNumbersCount != utilisedNumbersCount {
            seedB.restore(utilisedNumbersCount: utilisedNumbersCount)
        }
    }
    
}
