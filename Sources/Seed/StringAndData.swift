//
//  StringAndData.swift
//  CommonElements
//
//  Created by Jan Mazurczak on 21/08/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//

import Foundation


extension Data {
    
    public func hexString() -> String {
        map {
            String(format: "%02.2hhx", $0)
        }
        .joined()
        .lowercased()
    }
    
}


extension String {
    
    public func data() -> Data {
        return self.data(using: String.Encoding.utf8)!
    }
    
    
    public static func randomAllowedCharacters() -> String {
        return "abcdefghijklmnoprquwvxyzABCDEFGHIJKLMNOPRQUWVXYZ1234567890"
    }
    
    
    public static func randomAllowedCharacterSet() -> CharacterSet {
        return CharacterSet(charactersIn: randomAllowedCharacters())
    }
    
    
    public static func random(_ length: Int) -> String {
        return randomAllowedCharacters().random(length)
    }
    
    
    public func random(_ length: Int) -> String {
        let allCharacters = Array(self)
        let randomCharacters = allCharacters.random(length)
        return String(randomCharacters)
    }
    
}


public func +(leftValue: Data, rightValue: String) -> Data {
    return leftValue + rightValue.data(using: String.Encoding.utf8)!
}
