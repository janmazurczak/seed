//
//  IntegerAndData.swift
//  CommonElements
//
//  Created by Jan Mazurczak on 21/08/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//

import Foundation


extension BinaryInteger {
    
    public func normalised<RangeBound: BinaryInteger>(by range: CountableRange<RangeBound>) -> Int {
        return Int(normalisedIntMax(by: range))
    }
    
    public func normalisedIntMax<RangeBound: BinaryInteger>(by range: CountableRange<RangeBound>) -> Int64 {
        guard range.upperBound > range.lowerBound else {
            return Int64(range.lowerBound)
        }
        let source: Int64
        if let s = self as? UInt64 { // prevent crash when there is too few bits to represent the source
            source = Int64(s/2)
        } else {
            source = Int64(self)
        }
        let from = Int64(range.lowerBound)
        let to = Int64(range.upperBound)
        let result = from + (source % (to - from))
        return result
    }
    
    /// Remember to respect Endianness - it writes bytes at default order on the platform
    public func data() -> Data {
        var int = self
        return Data(bytes: &int, count: MemoryLayout<Self>.size)
    }
    
}


extension Data {
    
    /// Remember to respect Endianness - it writes bytes at default order on the platform
    public func toInteger<T: BinaryInteger>() -> T {
        let result = withUnsafeBytes { $0.load(as: T.self) }
        return result
    }
    
}


/// Remember to respect Endianness - it writes bytes at default order on the platform
public func +<T: BinaryInteger>(leftValue: Data, rightValue: T) -> Data {
    return leftValue + rightValue.data()
}
