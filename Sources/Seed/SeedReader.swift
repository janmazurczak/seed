// Copyright: Jan Mazurczak 2019

public struct SeedReader<Source, Output> where Source: SeedSource {
    public let numbersNeeded: Int
    public let outputConverter: ([Source]) -> Output
    
    public init(numbersNeeded: Int, outputConverter: @escaping ([Source]) -> Output) {
        self.numbersNeeded = numbersNeeded
        self.outputConverter = outputConverter
    }
    
    public func applying<ModifiedOutput>(modificator: @escaping (Output) -> ModifiedOutput) -> SeedReader<Source, ModifiedOutput> {
        let originalConverter = outputConverter
        return SeedReader<Source, ModifiedOutput>(numbersNeeded: numbersNeeded) {
            let originalOutput = originalConverter($0)
            return modificator(originalOutput)
        }
    }
}
