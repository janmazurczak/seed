//#-hidden-code
// Copyright: Jan Mazurczak 2019

extension SeedReader where Source == Output {
    public static var rawSource: SeedReader {
        return SeedReader(numbersNeeded: 1) {
            return $0[0]
        }
    }
}

extension SeedReader where Output == Bool {
    public static var bool: SeedReader {
        return SeedReader(numbersNeeded: 1) {
            return $0[0] % 2 == 0
        }
    }
}

extension SeedReader where Output == Float {
    public static var float0to1Converter: (([Source]) -> Float) {
        let max = Float(UInt64(Source.max))
        return {
            let value = $0[0]
            return Float(UInt64(value)) / max
        }
    }
    
    public static var float0to1: SeedReader {
        return SeedReader(numbersNeeded: 1, outputConverter: float0to1Converter)
    }
}

extension SeedReader where Output == Int {
    public static func int(in range: Range<Int>) -> SeedReader {
        return SeedReader(numbersNeeded: 1, outputConverter: {
            let value = $0[0]
            return value.normalised(by: range)
        })
    }
}

extension SeedsBridgeReader where Output == Float {
    public static var float0to1: SeedsBridgeReader {
        return SeedsBridgeReader(.float0to1, bridgeCombining: { (($1 - $0) * Float($2)) + $0 })
    }
}
