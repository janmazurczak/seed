import XCTest

import SeedTests

var tests = [XCTestCaseEntry]()
tests += SeedTests.allTests()
XCTMain(tests)
