# Seed

Usualy it goes like this:
```
let seed = Seed<UInt64>(string: "seedstring")
let a = seed.read(.float0to1)
let b = seed.read(.int(in: 0..<10))
let c = seed.read(.bool)
let d = seed.read(.rawSource)
```

Some example about restoring a state of the seed:
```
let seed1a = Seed<UInt64>(string: "seedstring1")
let seed1b = Seed<UInt64>(string: "seedstring1")
seed1a.read(.float0to1, amount: 513)

let utilised = seed1a.utilisedNumbersCount

seed1b.read(.float0to1, amount: 34)
seed1b.restore(utilisedNumbersCount: utilised)

let a = seed1a.read(.float0to1)
let b = seed1b.read(.float0to1)

let success = a == b

seed1a.read(.float0to1, amount: 208)
seed1b.restore(utilisedNumbersCount: seed1a.utilisedNumbersCount)

let c = seed1a.read(.float0to1)
let d = seed1b.read(.float0to1)

let success2 = c == d
```

Time Seed... what?
```
let float0to1 = SeedsBridgeReader<UInt32, Float>.float0to1
let date = Date()

let exactSeed = TimeSeed<UInt32>(sliceDuration: 20, modifier: "lol", date: date)
let fromSeed = exactSeed.updated(to: [Double(exactSeed.segmentEdges[0].0)])
let toSeed = exactSeed.updated(to: [Double(exactSeed.segmentEdges[0].1)])

let nexactSeed = TimeSeed<UInt32>(sliceDuration: 20, modifier: "lol", date: date.addingTimeInterval(20))
let nfromSeed = nexactSeed.updated(to: [Double(nexactSeed.segmentEdges[0].0)])
let ntoSeed = nexactSeed.updated(to: [Double(nexactSeed.segmentEdges[0].1)])

let valuesA = (0...8).map { _ in
    fromSeed.read(float0to1)
    } + [toSeed.read(float0to1)]

toSeed.restore(utilisedNumbersCount: fromSeed.utilisedNumbersCount)
let nextA = toSeed.read(float0to1)
nfromSeed.restore(utilisedNumbersCount: fromSeed.utilisedNumbersCount)
let nextB = nfromSeed.read(float0to1)

let success = nextA == nextB
```

Multidimensional Seed is something... beyond. So let's see it animated in 3D.
```
import SceneKit

let scene = SCNScene()
let view = SCNView()
view.scene = scene
view.allowsCameraControl = true
view.autoenablesDefaultLighting = true

let seed = MultidimensionalSeedsBridge<UInt32>(dimensions: [
    DimensionInfo(sliceLength: 3, defaultPosition: 0),
    DimensionInfo(sliceLength: 3, defaultPosition: 0),
    DimensionInfo(sliceLength: 3, defaultPosition: 0),
    DimensionInfo(sliceLength: 3, defaultPosition: 0)
    ], seedModifier: "wtf is happening here?", position: [])

let x = 5
let y = 5
let z = 5

scene.rootNode.addMultidimensionalVisualisation(xCount: x, yCount: y, zCount: z)
let action = SCNAction.multidimensionalVisualisationTimeTravel(seed: seed, xCount: x, yCount: y, zCount: z, duration: 1000)
scene.rootNode.runAction(action)

// present the view and 🤯
```
Oh and you need these helpers:
```
extension SCNNode {
    
    public func addMultidimensionalVisualisation(xCount: Int, yCount: Int, zCount: Int) {
        for x in 0..<xCount {
            for y in 0..<yCount {
                for z in 0..<zCount {
                    let size = CGFloat(1)
                    let radius = CGFloat(0)
                    let box = SCNNode(geometry: SCNBox(width: size, height: size, length: size, chamferRadius: radius))
                    box.geometry?.firstMaterial = SCNMaterial()
                    box.name = "\(x),\(y),\(z)"
                    box.position = SCNVector3(x, y, z)
                    addChildNode(box)
                }
            }
        }
    }
    
}

extension SCNAction {
    
    public static func multidimensionalVisualisationTimeTravel(seed: MultidimensionalSeedsBridge<UInt32>, xCount: Int, yCount: Int, zCount: Int, duration: TimeInterval) -> SCNAction {
        let float = SeedsBridgeReader<UInt32, Float>.float0to1
        return SCNAction.customAction(duration: duration) { (node, t) in
            for x in 0..<xCount {
                for y in 0..<yCount {
                    for z in 0..<zCount {
                        
                        seed.reset()
                        seed.update(to: [x, y, z].map{ Double($0) } + [Double(t)])
                        
                        let size = CGFloat(seed.read(float))
                        let radius = CGFloat(seed.read(float)) * 0.5
                        let box = node.childNode(withName: "\(x),\(y),\(z)", recursively: false)
                        box?.scale = SCNVector3(size, size, size)
                        (box?.geometry as? SCNBox)?.chamferRadius = radius
                        box?.geometry?.firstMaterial?.diffuse.contents = UIColor(
                            hue: CGFloat(seed.read(float)),
                            saturation: 0.5,
                            brightness: 0.5,
                            alpha: 1)
                        box?.position.x = Float(x) + seed.read(float) - 0.5
                        box?.position.y = Float(y) + seed.read(float) - 0.5
                        box?.position.z = Float(z) + seed.read(float) - 0.5
                    }
                }
            }
        }
    }
    
}
```
